from peewee import *
from modelHistory import *
from entGameHistory import *
from util import *
from const import *



def create(game: GameHistory):
	Game.create(name=game.name, date=game.date)


def addPlayer(gamePlayer: GamePlayer):
	GamePlayerHistory.create(gameHistory=gamePlayer.idGameHistory, idPlayer=gamePlayer.idPlayer, idHero= gamePlayer.idHero,side=gamePlayer.side)
