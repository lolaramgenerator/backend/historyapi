from peewee import *
from const import db
from modelHistory import *




def create_tables():
    with db:
        db.create_tables([GameHistory,GamePlayerHistory])

create_tables()


