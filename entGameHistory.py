from pydantic import BaseModel
from typing import List, Optional


class GameHistory(BaseModel):
	name: Optional[str] = None
	date: Optional[date] = None


