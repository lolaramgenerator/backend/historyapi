from pydantic import BaseModel
from typing import List, Optional


class GamePlayerHistory(BaseModel):
	idGameHistory: Optional[int] = None
	idPlayer: Optional[int] = None
	idHero: Optional[int] = None
	side: Optional[str] = None
