from peewee import *
from modelBase import *


class GameHistory(BaseModel):
	name = CharField()
	date = DateField()




class GamePlayerHistory(BaseModel):
	gameHistory = ForeignKeyField(GameHistory, backref='GamePlayersHistory')
	idPlayer = IntegerField()
	idHero = IntegerField()
	side = CharField()

